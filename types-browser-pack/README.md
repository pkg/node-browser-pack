# Installation
> `npm install --save @types/browser-pack`

# Summary
This package contains type definitions for browser-pack (https://github.com/substack/browser-pack).

# Details
Files were exported from https://github.com/DefinitelyTyped/DefinitelyTyped/tree/master/types/browser-pack.

### Additional Details
 * Last updated: Mon, 06 Nov 2023 22:41:05 GMT
 * Dependencies: [@types/node](https://npmjs.com/package/@types/node)

# Credits
These definitions were written by [TeamworkGuy2](https://github.com/TeamworkGuy2).
